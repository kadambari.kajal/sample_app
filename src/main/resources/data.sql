INSERT INTO `department` (`id`, `department_name`) VALUES ('1', 'Accounting');
INSERT INTO `department` (`id`, `department_name`)  VALUES (2, 'Finance');
INSERT INTO `department` (`id`, `department_name`)  VALUES (3, 'Human Resources');
INSERT INTO `department` (`id`, `department_name`)  VALUES (4, 'Marketing');
INSERT INTO `department` (`id`, `department_name`)  VALUES (5, 'Production');
INSERT INTO `department` (`id`, `department_name`)  VALUES (6, 'Research and Development');

INSERT INTO `employee` (id, created_by, created_time, updated_by, updated_time, department_id, dob, firstname, lastname, manager, salary) values (101, null, '2023-04-01 18:44:30.616574', null, '2023-04-01 18:44:30.616574', 1, '2001-04-01', 'Ajay', 'Thakur', null, '30000');
INSERT INTO `employee` (id, created_by, created_time, updated_by, updated_time, department_id, dob, firstname, lastname, manager, salary) values (102, null, '2023-04-01 18:44:30.616574', null, '2023-04-01 18:44:30.616574', 1, '2002-04-01', 'Radha', 'Sharma', null, '40000');