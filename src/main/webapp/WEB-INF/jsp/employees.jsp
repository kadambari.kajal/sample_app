<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html lang="en">
<head>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>
<body>
	<div class="container">
		<h2>Employee List</h2>
		<table class="table">
			<thead>
				<tr>
					<th scope="col">First Name</th>
					<th scope="col">Last Name</th>
					<th scope="col">DOB</th>
					<th scope="col">Department</th>
					<th scope="col">Manager</th>
					<th scope="col">Salary</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="employee" items="${employees}">
					<tr>
						<td><a href="${pageContext.request.contextPath}/employee/view/${employee.employeeId}">${employee.firstname}</a></td>
						<td>${employee.lastname}</td>
						<td>${employee.dob}</td>
						<td>${employee.departmentName}</td>
						<td>${employee.managerName}</td>
						<td>${employee.salary}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br />
	</div>

</body>

</html>