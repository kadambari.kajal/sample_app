package com.employee.management.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {

	private String DATE_FORMAT = "d/MM/yyyy"; 
	
	
	public LocalDate stringToLocalDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT); 
		// convert String to LocalDate
		LocalDate localDate = LocalDate.parse(date, formatter);
		return localDate;
	}

	public String localDateToString(LocalDate localDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT); 
		String format = localDate.format(formatter);
		return format;
	}
}
