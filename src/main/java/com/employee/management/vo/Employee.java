package com.employee.management.vo;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode
@EntityListeners(AuditingEntityListener.class)
@Table(name = "EMPLOYEE")
public class Employee extends Auditable<String> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false, length = 10)
	private Long id;

	@NaturalId(mutable = true)
	@Column(name = "firstname", unique = false, nullable = false, length = 75)
	private String firstname;

	@Column(name = "lastname", unique = false, nullable = true, length = 75)
	private String lastname;

	@Column(name = "salary")
	private Double salary;

	@Column(name = "manager", nullable = true)
	private Long manager;

	@ManyToOne
	@JoinColumn(name = "department_id")
	private Department department;

	@CreatedDate
	@Column(name = "DOB", updatable = false)
	private LocalDate dob;

}
