package com.employee.management.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.employee.management.utils.DateUtils;

@Configuration
public class BeanConfig {

	@Bean
	public DateUtils dateUtils() {
		return new DateUtils();
	}
}
