# Checkout project

 git clone git@gitlab.com:kadambari.kajal/sample_app.git 

# Build Steps (using Maven)

mvn clean install

# Run

java -jar java-coding-test.war

# Test App

http://localhost:8080/java-coding-test/welcome 
